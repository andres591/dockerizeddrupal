# README

Dockerized version of Drupal to run on local environment.

## Requirements

- Python3
- Docker
- Docker-compose

## Instalation

```
python3 start.py [drupal-version]
```

The Drupal version must the name of a zip at the root of this project, for example.

```
python3 start.py drupal-8.9.13
```

Then you can go to [http://localhost:8081](http://localhost:8081) and start the configuration of Drupal itself

(when setting up the database, you have to change the hostname under advanced options to the container name of the DB, in this case `drupal_db`).

To start Jenkins you have to go to [http://localhost:8080](http://localhost:8080) and follow the installation steps.

## Customization

You can take a look the `docker-compose-base.yml` file at the root of this project.

The `docker-compose.yml` file is generated, so changing it wont take any effect on the result.

## Uninstall

```
docker-compose down
```

Then remove the folder with the name of the version of Drupal you are running.
