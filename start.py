import sys
import subprocess
import fileinput

subprocess.run(["sudo", "docker-compose", "down"])

version = sys.argv[1]

folder = version
filename = version + ".zip"

subprocess.check_output(["unzip", "-o", filename])

with open("docker-compose-base.yml") as file:
    data = file.read()
    data = data.replace(r"{{drupal_version}}", folder)
    file.close()

subprocess.check_output(["mkdir", "-p", f"./{folder}/sites/default/files"])
subprocess.check_output(["cp", f"./{folder}/sites/default/default.settings.php", f"./{folder}/sites/default/settings.php"])
subprocess.check_output(["sudo", "chmod", "777", "-R", f"./{folder}"])

with open('docker-compose.yml', 'w') as file:
  file.write(data)
  file.close()

subprocess.check_output(["sudo", "docker-compose", "up", "-d"])